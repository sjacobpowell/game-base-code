package com.sjacobpowell.game.art;

import com.sjacobpowell.game.core.Game;

public abstract class Screen extends Bitmap {

	public Screen(int width, int height) {
		super(width, height);
	}

	abstract public void render(Game game);
}
