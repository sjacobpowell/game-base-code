package com.sjacobpowell.game.core;

public abstract class Game {
	public int time;
	protected int width;
	protected int height;

	public Game(int width, int height) {
		this.width = width;
		this.height = height;
	}
	public abstract void tick(boolean[] keys);
}
