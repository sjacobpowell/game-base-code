package com.sjacobpowell.game.core;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.sjacobpowell.game.art.Screen;
import com.sjacobpowell.game.io.InputManager;

public class GameComponent extends Canvas implements Runnable {
	protected static final long serialVersionUID = 1L;
	public static int WIDTH = 320;
	public static int HEIGHT = 200;
	public static int SCALE = 2;
	protected boolean running = false;
	protected Thread thread;
	protected Game game;
	protected Screen screen;
	protected InputManager inputManager;
	protected BufferedImage image;
	protected int[] pixels;
	protected static final String OS = System.getProperty("os.name").toLowerCase();

	public GameComponent() {
			Dimension size = new Dimension(WIDTH * SCALE, HEIGHT * SCALE);
		if(OS.indexOf("win") >= 0) {
			size = new Dimension(WIDTH * SCALE - 10, HEIGHT * SCALE - 10);
		}
		setSize(size);
		setPreferredSize(size);
		setMinimumSize(size);
		setMaximumSize(size);

		game = new Game(WIDTH, HEIGHT) {
			@Override
			public void tick(boolean[] keys) {
				time++;
			}
		};
		screen = new Screen(WIDTH, HEIGHT) {
			@Override
			public void render(Game game) {
			}
		};

		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

		inputManager = new InputManager();
		addKeyListener(inputManager);
		setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB), new Point(0, 0), "empty"));
	}

	@Override
	public void run() {
		int tickCount = 0;
		double FPS = 60.0;
		double unprocessedSeconds = 0;
		double secondsPerTick = 1 / FPS;
		double end = System.nanoTime();

		requestFocus();

		while (running) {
			double start = System.nanoTime();
			double passedTime = start - end;
			end = start;
			if (passedTime < 0)
				passedTime = 0;
			if (passedTime > 100000000)
				passedTime = 100000000;

			unprocessedSeconds += passedTime / 1000000000;

			boolean ticked = false;
			while (unprocessedSeconds > secondsPerTick) {
				tick();
				unprocessedSeconds -= secondsPerTick;
				ticked = true;

				tickCount++;
				if (tickCount % FPS == 0) {
					end += 1000;
				}
			}

			if (ticked) {
				render();
			} else {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void tick() {
		game.tick(inputManager.keys);
	}

	private void render() {
		BufferStrategy strategy = getBufferStrategy();
		if (strategy == null) {
			createBufferStrategy(3);
			return;
		}

		screen.render(game);
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen.pixels[i];
		}

		Graphics g = strategy.getDrawGraphics();
		g.drawImage(image, 0, 0, WIDTH * SCALE, HEIGHT * SCALE, null);
		g.dispose();
		strategy.show();
	}

	public void start() {
		if (running)
			return;
		running = true;
		thread = new Thread(this);
		thread.start();
	}

	public void stop() {
		if (!running)
			return;
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		GameComponent game = new GameComponent();
		JFrame frame = new JFrame("Game");
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(game, BorderLayout.CENTER);
		frame.setContentPane(panel);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setVisible(true);
		game.start();
	}
}
